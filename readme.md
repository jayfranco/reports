## Purpose
This tool get an all orders report from Amazon MWS and writes it into Azure SQL. PowerBI then uses this data.

## How to Run
Thanks to [zerogjoe/mssql-python3.6-pyodbc](https://hub.docker.com/r/zerogjoe/mssql-python3.6-pyodbc/dockerfile).
```
#required is 1 file, some todo here
cd /tmp
echo 'echo "entrypoint works"' > entrypoint.sh

#the 1-liner
docker run -v ~/jfs/reports:/sample -it zerogjoe/mssql-python3.6-pyodbc /bin/bash

#if already had run above ever, you can use the image
docker start `docker images -q|sed '$!d'`
docker exec -it `docker ps -q -n1` bash
```

# Azure SQL
Ran the below on our db:
```sql
CREATE TABLE "orders_0000"
(
"AmazonOrderID" varchar(80),
"MerchantOrderID" varchar(80),
"PurchaseDate" smalldatetime,
"LastUpdatedDate" smalldatetime,
"OrderStatus" varchar(80),
"SalesChannel" varchar(80),
"FulfillmentChannel" varchar(80),
"ShipServiceLevel" varchar(80),
"IsBusinessOrder" bit not null,
"AmazonOrderItemCode" varchar(80),
"ASIN" varchar(80),
"SKU" varchar(80),
"ItemStatus" varchar(80),
"ProductName" text,
"Quantity" smallint,
"City" varchar(80),
"State" varchar(80),
"PostalCode" varchar(80),
"Country" varchar(80),
);

CREATE TABLE "orders_currency_0000"
(
"AmazonOrderID" varchar(80),
"CurrencyType" varchar(80),
"CurrencyUnit" varchar(80),
"CurrencyValue" DECIMAL(7,2),
"ASIN" varchar(80),
)

ALTER TABLE
orders_currency_0000
ADD COLUMN
"ASIN" varchar(80)
```
Useful Queries
```
#get monthly order counts
SELECT
CAST(MONTH(PurchaseDate) AS VARCHAR(2)) + '-' + CAST(YEAR(PurchaseDate) AS VARCHAR(4)) AS "Month",
COUNT(*) AS "Monthly Orders"
FROM orders_0000
GROUP BY CAST(MONTH(PurchaseDate) AS VARCHAR(2)) + '-' + CAST(YEAR(PurchaseDate) AS VARCHAR(4))
ORDER BY 1 DESC

#check a date range's data in a date range
SELECT * FROM orders_0000
WHERE PurchaseDate > '20180712 00:00:00.000'
AND PurchaseDate < '20180813 00:00:00.000'

#how many orders in a date range
SELECT COUNT(*) FROM orders_0000
WHERE PurchaseDate > '20180413 00:00:00.000'
AND PurchaseDate < '20180513 00:00:00.000'

#check each month as loaded
SELECT
TOP 10
*
FROM orders_0000
INNER JOIN orders_currency_0000
ON orders_0000.AmazonOrderID = orders_currency_0000.AmazonOrderID
AND orders_0000.ASIN = orders_currency_0000.ASIN
WHERE orders_0000.PurchaseDate > '20180413 00:00:00.000'
AND orders_0000.PurchaseDate < '20180513 00:00:00.000'

#test 1 shipment
SELECT *
FROM orders_0000
LEFT JOIN orders_currency_0000
ON orders_0000.AmazonOrderID = orders_currency_0000.AmazonOrderID
AND orders_0000.ASIN = orders_currency_0000.ASIN
WHERE orders_0000.AmazonOrderID='114-0738237-5205053'

SELECT *
FROM orders_0000
WHERE AmazonOrderID='114-0738237-5205053'

SELECT *
FROM orders_currency_0000
WHERE AmazonOrderID='114-0738237-5205053'


```
