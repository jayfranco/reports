#only for future after alpine support of ms odbc
FROM python
RUN curl https://packages.microsoft.com/keys/microsoft.asc | apt-key add -
RUN curl https://packages.microsoft.com/config/debian/10/prod.list > /etc/apt/sources.list.d/mssql-release.list
RUN apt-get update
RUN ACCEPT_EULA=Y apt-get install msodbcsql17
ENV LANG en_US.UTF-8
RUN pip install --no-cache-dir pyyaml requests
RUN pip install --no-cache-dir lxml beautifulsoup4