#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# JFranco MWS service

import re
import hashlib
import hmac
import base64
import requests
from urllib import parse
from urllib.error import URLError, HTTPError
import datetime

url_config = {
    'domain': 'https://mws.amazonservices.com',
    'Fulfillment': {
        'path': '/FulfillmentInboundShipment/2010-10-01',
    },
    'Reports': {
        'path': '/Reports/2009-01-01',
        'version': '2009-01-01',
    },
    'Feed': {
        'path': '/',
        'version': '2009-01-01',
    },
}

class MWSError(Exception):
    print(Exception)

class MWS:
    def __init__(self, access_key, secret_key, merchant_id, marketplace_id, section, operation, params, body = ''):
        self.access_key = access_key
        self.secret_key = secret_key
        self.merchant_id = merchant_id
        #self.marketplace_id = marketplace_id
        self.domain = url_config.get('domain')
        self.user_agent = ''
        self.params = params
        self.url_path = url_config.get(section).get('path', '')
        self.operation = operation
        self.version = url_config.get(section).get('version', '2010-10-01')
        self.body = body

    def call(self):
        #call Amazon MWS
        params = {
            'AWSAccessKeyId': self.access_key,
            'SellerId': self.merchant_id, #todo: configure for Merchant vs SellerId?
            'Action': self.operation, 
            'SignatureMethod': 'HmacSHA256',
            'SignatureVersion': '2',
            'Timestamp': datetime.datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%SZ'),
            'Version': self.version,
            }
        params.update(self.params)
        #todo: sanitize core params
        #todo: test order of args for both GET and POST
        if(self.body):
            params['Merchant'] = params.pop('SellerId')
        request_description = '&'.join(['%s=%s' % (k, str(parse.quote_plus(str(params[k]), safe='-_.~')).replace('+', '%20')) for k in sorted(params)])
        signature = self.calc_signature(request_description)
        params.update({'Signature': signature})
        if('ReportRequest' == self.operation):
            return '117378018101'
        if('GetReportRequestList' == self.operation):
            return '15860639976018101'
            #import time
            #time.sleep(15)
        try:
            if(self.params):
                #if feed, add feed as body to request
                if(self.body):
                    response = requests.post(self.domain + self.url_path, params=params, data=self.body, headers={'Content-type': 'text/xml'})
                else:
                    response = requests.post(self.domain + self.url_path, data=params)
        except URLError as error:
            #print(error.hdrs)
            #print(error.fp)
            output_file = open(signature + ' error.xml', 'w')
            output_file.write(request_description)
            output_file.write(signature)
            output_file.write(reponse)
            output_file.close()
            return(error.msg)
        return response.text

    def calc_signature(self, request_description, path = '/'):
        #calculate MWS signature to interface with Amazon
        method = 'POST' if (self.params) else 'GET'
        sig_data = method + '\n' + self.domain.replace('https://', '').lower() + '\n' + self.url_path + '\n' + request_description
        return base64.b64encode(hmac.new(bytes(self.secret_key, 'latin-1'), bytes(sig_data, 'latin-1'), hashlib.sha256).digest())


    def xml_to_dict(self, xml):
        try:
            dom = minidom.parseString(xml)
        except:
            #not XML, return text
            return xml
        else:
            return 'neither XML nor text'
