import yaml
from mws import MWS
from bs4 import BeautifulSoup
import time
import datetime

#load MWS configuration
conf = yaml.load(open('conf/mws.yml'))
base_params = {
'access_key': conf['mws']['access_key'],
'secret_key': conf['mws']['secret_key'],
'merchant_id': conf['mws']['merchant_id'],
'marketplace_id': conf['mws']['marketplace_id'],
}

#start db once
import pyodbc
server = 'jfreports.database.windows.net'
database = 'reports'
username = 'bonfirewt'
password = 'Zfanzy87314v'
driver= '{ODBC Driver 17 for SQL Server}'
cnxn = pyodbc.connect('DRIVER='+driver+';SERVER='+server+';PORT=1433;DATABASE='+database+';UID='+username+';PWD='+ password)
cursor = cnxn.cursor()

#date range generator
from datetime import timedelta, date
import math
def daterange(start_date, end_date):
    for n in range(math.ceil ( int ((end_date - start_date).days) / 30) ):
        yield {
        'start_date': start_date + timedelta(days = ((n-1)*30)),
        'end_date': start_date + timedelta(days = (n*30)),
        }

def request_report(dt):
    #request report, get request_report_id
    request_report_params = {
    'ReportType': '_GET_XML_ALL_ORDERS_DATA_BY_ORDER_DATE_',
    'StartDate': dt.get('start_date').strftime("%Y-%m-%dT00:00:00Z"),
    'EndDate': dt.get('end_date').strftime("%Y-%m-%dT00:00:00Z"),
    }
    request_report = MWS(
    access_key=conf['mws']['access_key'],
    secret_key=conf['mws']['secret_key'],
    merchant_id=conf['mws']['merchant_id'],
    marketplace_id=conf['mws']['marketplace_id'],
    section='Reports',
    operation='RequestReport',
    params=request_report_params)
    request_report_response = request_report.call()
    request_report_response_soup = BeautifulSoup(request_report_response, 'xml')
    return request_report_response_soup.ReportRequestId.string

#check report_request status, get report_id
def request_to_report(report_request_id):
    get_report_list_params = {
    'ReportRequestIdList.Id.1': report_request_id
    }
    get_report_list = MWS(
    access_key=conf['mws']['access_key'],
    secret_key=conf['mws']['secret_key'],
    merchant_id=conf['mws']['merchant_id'],
    marketplace_id=conf['mws']['marketplace_id'],
    section='Reports',
    operation='GetReportList',
    params=get_report_list_params)
    get_report_list_response = get_report_list.call()
    get_report_list_response_soup = BeautifulSoup(get_report_list_response, 'xml')
    if(get_report_list_response_soup.ReportId):
        #wait until time.now() >= time(get_report_list_response_soup.AvailableDate.string)):
        return get_report_list_response_soup.ReportId.string
    return False

def get_report(report_id):
    filename = 'del//report ' + str(report_id) + '.xml'
    try:
        with open(filename, 'r', encoding='utf-8') as file:
            file_read = file.read().replace('\n', '')
        report_read = BeautifulSoup(file_read, 'xml')
        print('Read ' + filename + '\'s ' + str(len(report_read.findAll('Message'))) + ' orders')
        file.close()
        return report_read
    except IOError:
        print('Report ' + str(report_id) + ' wasn\'t saved, get from MWS.')
        get_report_params = base_params
        get_report_params.update({
        'ReportId': report_id,
        })
        get_report = MWS(
            access_key=conf['mws']['access_key'],
            secret_key=conf['mws']['secret_key'],
            merchant_id=conf['mws']['merchant_id'],
            marketplace_id=conf['mws']['marketplace_id'],
            section='Reports',
            operation='GetReport',
            params=get_report_params)
        get_report_response = get_report.call()
        get_report_response_encoded = get_report_response.encode(errors='ignore')
        output_file = open(filename, 'wb')
        output_file.write(get_report_response_encoded)
        output_file.close()
        return BeautifulSoup(get_report_response_encoded, 'xml')

def write_db(report_soup, additional = False):
    fields = [
    'AmazonOrderID',
    'MerchantOrderID',
    'PurchaseDate',
    'LastUpdatedDate',
    'OrderStatus',
    'SalesChannel',
    'FulfillmentChannel',
    'ShipServiceLevel',
    'IsBusinessOrder',
    'AmazonOrderItemCode',
    'ASIN',
    'SKU',
    'ItemStatus',
    'ProductName',
    'Quantity',
    'City',
    'State',
    'PostalCode',
    'Country',
    ]
    query_currency = 'INSERT INTO orders_currency_0000 VALUES(' + ','.join(['?'] * 5) + ')'
    query = 'INSERT INTO orders_0000 VALUES(' + ','.join(['?']*len(fields)) + ')'
    #output_file = open('query.xml', 'wb')
    #@todo: use config mapper
    for order in report_soup.findAll('Message'):
        if('Message' == order.name):
            for item in order.findAll('OrderItem'):
                data = []
                for field in fields:
                    if(item.find(field) or order.find(field)):
                        if(field in ['ASIN','SKU','ItemStatus','ProductName','Quantity']):
                            value = str(item.find(field).string).strip()
                        else:
                            value = str(order.find(field).string).strip()
                        if('IsBusinessOrder' == field):
                            data.append('1' if 'true' == value else '0')
                        elif('Date' in field):
                            data.append(datetime.datetime.strptime(value, '%Y-%m-%dT%H:%M:%S+00:00').strftime('%Y-%-m-%-d %H:%M %p'))
                            #data.append("CONVERT(datetime, '" + value.replace('+00:00', 'Z') + "', 127)")
                        else:
                            data.append(value)
                    else:
                        data.append(None)
                #output_file.write(query.encode())
                cursor.execute(query, data)
                for price in item.findAll('ItemPrice'):
                    data_currency = [
                    order.AmazonOrderID.string,
                    price.Type.string, #CurrencyType
                    price.Amount.get('currency', ''), #CurrencyUnit
                    float(price.Amount.string), #CurrencyValue
                    item.ASIN.string,
                    ]
                    cursor.execute(query_currency, data_currency)
    cnxn.commit()
    #output_file.close()
    return True

def add_items(report_ids):
    for report_id in report_ids:
        report_soup = get_report(report_id)
        report_written = write_db(report_soup)

start_date = date(2018, 3, 14)
end_date = date(2019, 12, 4)
for dt in daterange(start_date, end_date):
    report_request_id = request_report(dt)
    print('Get report for orders ' + start_date.strftime("%Y-%m-%d") + '-' + end_date.strftime("%Y-%m-%d"))
    report_id = request_report(dt)
    report_id = request_to_report(report_request_id)
    print('Write ReportId', end='=')
    print(report_id)
    report_soup = get_report(report_id)
    report_written = write_db(report_soup)
cnxn.close()